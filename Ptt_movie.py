import requests
from bs4 import BeautifulSoup
import sqlite3

def creat_table():
    try:
        # 連接資料庫
        conn = sqlite3.connect('DB.db')
        cursor = conn.cursor()
        
        # 表格不存在則創建
        create_table_article = f'''
        CREATE TABLE IF NOT EXISTS article (
        	id INTEGER PRIMARY KEY,
            title TEXT,
            textBody TEXT
        )
        '''
        cursor.execute(create_table_article)
        conn.commit()
        
        create_table_responseText = f'''
        CREATE TABLE IF NOT EXISTS response (
        	id INTEGER PRIMARY KEY,
            article_id INTEGER,
            responseText TEXT
        )
        '''
        cursor.execute(create_table_responseText)
        conn.commit()
    except sqlite3.Error as e:
        print(f"SQLite错误：{e}")
    finally:
        if conn:
            conn.close()

def get_ptt_text(): #爬取PTT
    index = 0 #用於計算現在爬了幾則
    moveurl = "https://www.ptt.cc/bbs/movie/index.html"
    while index<41: #最多不要超過40，總共有九千多頁，爬滿會花超長時間
        rQ=requests.get(moveurl).text
        souP=BeautifulSoup(rQ,"html5lib")
        nextpage = "https://www.ptt.cc"+souP.find_all('a', "btn wide")[1]['href']
        moveurl = nextpage
        try:
            conn = sqlite3.connect('DB.db')
            cursor = conn.cursor()
            for mySoup in souP.find_all("div","r-ent"):
                title = mySoup.find("div","title").text.strip() #文章列表(標題)
                if ("[movie]" in title) or ("[公告]" in title) or ("[版務" in title):
                    continue
                else:
                    try:
                        href = "https://www.ptt.cc"+mySoup.find("div","title").a["href"] #內文網址
                    except:
                        href = None
                    if href:
                        list_rq = requests.get(href).text
                        list_soup=  BeautifulSoup(list_rq,"html5lib")
                        textBody = list_soup.find("div","bbs-screen bbs-content") #內文
                        for metaline in textBody.find_all("div", "article-metaline"): #刪除不必要內容
                            metaline.decompose()
                        for metaline in textBody.find_all("div", "article-metaline-right"): #刪除不必要內容
                            metaline.decompose()
                        for metaline in textBody.find_all("span", "f2"): #刪除不必要內容
                            metaline.decompose()
                        for response in textBody.find_all("div", "push"): #取得回文
                            responseText = response.text.strip()
                            cursor.execute("INSERT OR REPLACE INTO response (article_id, responseText) VALUES (?, ?)", (index, responseText))
                            conn.commit()
                        for metaline in textBody.find_all("div", "push"): #回文取得後刪除
                            metaline.decompose()
                        textBody = textBody.text.strip()
                        cursor.execute("INSERT OR REPLACE INTO article (id, title, textBody) VALUES (?, ?, ?)", (index, title, textBody))
                        conn.commit()
                        index += 1
                        print(index)
                        if index > 41:
                            break
        except sqlite3.Error as e:
            print(f"SQLite错误：{e}")
        finally:
            if conn:
                conn.close()
                
creat_table()
get_ptt_text()