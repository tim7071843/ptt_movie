# -*- coding: utf-8 -*-

# Ptt_movie implementation generated from reading ui file 'ptt_ui.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import sqlite3

class Ui_Ptt_movie(object):
    def setupUi(self, Ptt_movie):
        Ptt_movie.setObjectName("Ptt_movie")
        Ptt_movie.resize(1200, 780)
        
        self.tableWidget = QtWidgets.QTableWidget(Ptt_movie)
        self.tableWidget.setGeometry(QtCore.QRect(100, 20, 1080, 650))
        self.tableWidget.setTabletTracking(False)
        self.tableWidget.setLineWidth(1)
        self.tableWidget.setAlternatingRowColors(False)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(1)
        self.tableWidget.setRowCount(1)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setItem(0, 0, item)
        
        self.pushButton = QtWidgets.QPushButton(Ptt_movie)
        self.pushButton.setGeometry(QtCore.QRect(20, 20, 60, 650))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.get_data)
        
        self.lineEdit = QtWidgets.QLineEdit(Ptt_movie)
        self.lineEdit.setGeometry(QtCore.QRect(20, 700, 1160, 50))
        font = QtGui.QFont()
        font.setFamily("Agency FB")
        font.setPointSize(14) #字體大小
        self.lineEdit.setFont(font)
        self.lineEdit.setStyleSheet("color: rgb(50, 50, 50)")
        self.lineEdit.setObjectName("lineEdit")
        self.lineEdit.returnPressed.connect(self.open_new_window) #輸入編碼，觸發open_new_window
        self.lineEdit.setPlaceholderText("輸入編號後enter") #預留文字

        self.retranslateUi(Ptt_movie)
        QtCore.QMetaObject.connectSlotsByName(Ptt_movie)

    def retranslateUi(self, Ptt_movie):
        _translate = QtCore.QCoreApplication.translate
        Ptt_movie.setWindowTitle(_translate("Ptt_movie", "Ptt_movie"))
        item = self.tableWidget.verticalHeaderItem(0)
        item.setText(_translate("Ptt_movie", "1"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("Ptt_movie", "標題"))
        __sortingEnabled = self.tableWidget.isSortingEnabled()
        self.tableWidget.setSortingEnabled(False)
        self.tableWidget.setSortingEnabled(__sortingEnabled)
        self.pushButton.setText(_translate("Ptt_movie", "抽取"))
        # self.lineEdit.setText(_translate("Ptt_movie", "輸入編號後enter"))
        
    def push_data(self, data): #印出標題
        for row in range(len(data)):
            self.tableWidget.setRowCount(row+1) 
            #data[row][0]=ID,data[row][1]=title,data[row][2]=內文
            item = QtWidgets.QTableWidgetItem(data[row][1])
            self.tableWidget.setItem(row, 0, item)
        self.tableWidget.resizeColumnsToContents()
        
    def get_data(self): #article，取得隨機15筆
        conn = sqlite3.connect('DB.db')
        cursor = conn.cursor()

        cursor.execute('SELECT * FROM article ORDER BY RANDOM() LIMIT 15')

        self.data = cursor.fetchall()

        conn.close()
        self.push_data(self.data)
    
    def open_new_window(self): #取得內文及回文，傳入新視窗
        input_id = self.lineEdit.text() #使用者輸入數字
        if input_id.isdigit() and 1 <= int(input_id) <= 15:
            title_id = self.data[int(input_id)-1][0] #文章ID
            title = self.data[int(input_id)-1][1] #文章title
            text_body = self.data[int(input_id)-1][2] #內文
            conn = sqlite3.connect('DB.db')
            cursor = conn.cursor()
            cursor.execute(f'SELECT * FROM response where article_id = {title_id}')
            response_text = cursor.fetchall() #此文章所有回文
            conn.close()
            self.new_window = NewWindow(title, text_body, response_text)
            self.new_window.show()
            self.lineEdit.clear()
        else:
            error_message = "請輸入數字，且大小在1到15之間"
            QtWidgets.QMessageBox.warning(Ptt_movie, "錯誤", error_message) #錯誤視窗
            

class NewWindow(QtWidgets.QWidget): #新視窗
    def __init__(self, title, text_body, response_text):
        super().__init__()
        self.setWindowTitle(f"{title}")
        self.setGeometry(100, 100, 1000, 600)
        self.textEdit = QtWidgets.QTextEdit(self)
        self.textEdit.setGeometry(QtCore.QRect(10, 10, 980, 290))
        self.textEdit.setObjectName("textEdit")
        self.textEdit.setReadOnly(True)
        self.textEdit.setText(text_body)
        self.response_list = QtWidgets.QListWidget(self)
        self.response_list.setGeometry(QtCore.QRect(10, 300, 980, 290))
        self.response_list.setObjectName("response_list")
        for response in response_text:
            new_item = QtWidgets.QListWidgetItem(response[2])
            self.response_list.addItem(new_item)
        

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Ptt_movie = QtWidgets.QWidget()
    ui = Ui_Ptt_movie()
    ui.setupUi(Ptt_movie)
    ui.get_data()
    Ptt_movie.show()
    sys.exit(app.exec_())

